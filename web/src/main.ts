import { createApp } from 'vue'
import App from "./App.vue"
import router from './router'
import store from './store'

import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'

//所有的图标库加载
import * as Icons from '@ant-design/icons-vue';

//统一配置axios的baseURL
import axios from 'axios';
axios.defaults.baseURL = process.env.VUE_APP_SERVER;

/**
 * axios拦截器
 * 请求拦截
 * 响应拦截
 */
axios.interceptors.request.use(function (config) {
    console.log('请求参数：', config);
    return config;
}, error => {
    return Promise.reject(error);
});
axios.interceptors.response.use(function (response) {
    console.log('返回结果：', response);
    return response;
}, error => {
    console.log('返回错误：', error);
    return Promise.reject(error);
});

// @ts-ignore
const app = createApp(App);
app.use(Antd).use(store).use(router).mount('#app')

//全局使用图标
const icons: any = Icons;
for (const i in icons) {
    app.component(i, icons[i])
}

console.log('环境:', process.env.NODE_ENV)