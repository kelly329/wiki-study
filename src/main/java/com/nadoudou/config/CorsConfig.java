package com.nadoudou.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//跨域解决的配置
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") //针对所有的接口地址
                .allowedOriginPatterns("*")
                .allowedHeaders(CorsConfiguration.ALL)
                .allowedMethods(CorsConfiguration.ALL) //get post 。。。
                .allowCredentials(true)//凭证 token或session
                .maxAge(3600); // 1小时内不需要再预检（发OPTIONS请求）
        //在调用接口之前会发一个options请求，检查接口是否存在是否有
    }

}
