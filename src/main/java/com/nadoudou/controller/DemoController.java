package com.nadoudou.controller;

import com.nadoudou.domain.Demo;
import com.nadoudou.domain.Test;
import com.nadoudou.service.DemoService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Resource
    private DemoService demoService;


    @RequestMapping("/list")
    public List<Demo> list(){
        return demoService.list();
    }



}
