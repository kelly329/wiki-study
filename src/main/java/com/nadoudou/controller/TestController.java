package com.nadoudou.controller;

import com.nadoudou.domain.Test;
import com.nadoudou.service.TestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {

    @Resource
    private TestService testService;

    //读yaml文件的配置项 :默认值 以配置文件优先
    @Value("${hello.test:Test}")
    private String helloTest;

    @RequestMapping("/hello")
    public String hello(){
        return "hello world!---" + helloTest;
    }

    @PostMapping("/hello")
    public String helloPost(String name){
        return "hello world!" + name;
    }

    @RequestMapping("/test/list")
    public List<Test> list(){
        return testService.list();
    }



}
