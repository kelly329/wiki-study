package com.nadoudou.controller;

import com.nadoudou.domain.Demo;
import com.nadoudou.domain.Ebook;
import com.nadoudou.requests.EbookReq;
import com.nadoudou.responses.CommonResp;
import com.nadoudou.responses.EbookResp;
import com.nadoudou.service.DemoService;
import com.nadoudou.service.EbookService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/ebook")
public class EbookController {

    @Resource
    private EbookService ebookService;


    @RequestMapping("/list")
    public CommonResp list(EbookReq req){
        CommonResp<List<EbookResp>> resp = new CommonResp<>();
        List<EbookResp> list = ebookService.list(req);
        resp.setContent(list);
        return resp;

    }





}
