package com.nadoudou.service;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.mysql.cj.util.StringUtils;
import com.nadoudou.domain.Demo;
import com.nadoudou.domain.Ebook;
import com.nadoudou.domain.EbookExample;
import com.nadoudou.mapper.DemoMapper;
import com.nadoudou.mapper.EbookMapper;
import com.nadoudou.requests.EbookReq;
import com.nadoudou.responses.EbookResp;
import com.nadoudou.utils.CopyUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class EbookService {

    @Resource
    private EbookMapper ebookMapper;

    public List<EbookResp> list(EbookReq req){
        EbookExample ebookExample = new EbookExample();
        //Critera 相当于where条件
        EbookExample.Criteria criteria = ebookExample.createCriteria();
        //动态sql写法
        if(!ObjectUtils.isEmpty(req.getName())){
            criteria.andNameLike("%"+req.getName()+"%");
        }
        List<Ebook> ebookList = ebookMapper.selectByExample(ebookExample);

        //持久层返回List<Ebook>需要转换为List<EbookResp>再返回给Controller

//        List<EbookResp> respList = new ArrayList<>();
//        for (Ebook ebook: ebookList) {
//            //对象的复制
////            EbookResp ebookResp = new EbookResp();
//////            ebookResp.setId(ebook.getId());
//////            ebook.setId(123l);
////            BeanUtils.copyProperties(ebook, ebookResp);
//
////            使用封装的单对象复制类
//            EbookResp ebookResp = CopyUtil.copy(ebook, EbookResp.class);
//            respList.add(ebookResp);
//        }
        //使用封装的列表复制
        List<EbookResp> respList = CopyUtil.copyList(ebookList, EbookResp.class);
        return respList;
    }

}
