package com.nadoudou.service;

import com.nadoudou.domain.Test;
import com.nadoudou.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestService {

    //    @Autowired //spring自带  resource //jdk自带
    @Resource
    private TestMapper testMapper;

    public List<Test> list(){
        return testMapper.list();
    }

}
