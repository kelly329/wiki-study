package com.nadoudou.mapper;

import com.nadoudou.domain.Test;

import java.util.List;

public interface TestMapper {

    public List<Test> list();
}
